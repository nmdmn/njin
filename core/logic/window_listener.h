#pragma once

namespace core::logic {
class WindowListener {
public:
	virtual void onSizeChanged(int width, int hegiht) = 0;
};
}
