#pragma once

#include "core/util/forward.h"

#include "core/render/uniform.h"
#include "core/render/shader.h"
#include "core/logic/window_listener.h"

namespace njRender = core::render;
namespace njLogic = core::logic;

namespace core::item {
class Camera : public njLogic::WindowListener {
public:
	void create();
	void setView(glm::mat4 view);
	void setProjection(glm::mat4 projection);
	void update(const njRender::Shader& shader);

	void onSizeChanged(int width, int height) override;

private:
	njRender::Uniform<glm::mat4> uView;
	njRender::Uniform<glm::mat4> uProjection;
};
}
