#include "camera.h"

void core::item::Camera::create() {
	uView.set("uView");
	uProjection.set("uProjection");
}

void core::item::Camera::setView(glm::mat4 view) {
	uView = view;
}

void core::item::Camera::setProjection(glm::mat4 projection) {
	uProjection = projection;
}

void core::item::Camera::update(const njRender::Shader& shader) {
	shader.set(uView);
	shader.set(uProjection);
}

void core::item::Camera::onSizeChanged(int width, int height) {
	uProjection = glm::perspective(45.0f, (float) width / height, 0.1f, 1000.0f);
}
