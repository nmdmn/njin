#pragma once

#define _USE_MATH_DEFINES

#define GLM_FORCE_RADIANS
#define GLM_ENABLE_EXPERIMENTAL

#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <functional>
#include <memory>
#include <initializer_list>
#include <random>
#include <numeric>
#include <limits>

#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>
#include <glm/glm.hpp>

namespace util {
typedef unsigned int glId;
}
