#pragma once

#include "core/util/forward.h"

namespace core::asset {
class Config {
public:
	glm::vec3 bgColor = glm::vec3(0.2f, 0.3f, 0.3f);
	glm::vec3 fgColor = glm::vec3(1.0f, 0.5f, 0.2f);

public:
	~Config();
	inline static Config& get() { static Config instance; return instance; }

private:
	Config();
	Config(Config const&) {}
	Config& operator=(Config const&) {}
};
}
