#include "shader.h"

#include "res.h"

namespace core::render {
struct ShaderHandle {
	ShaderHandle(util::glId program, util::glId shader) : mShader(shader) { glAttachShader(program, shader); }
	~ShaderHandle() { glDeleteShader(mShader); }
	util::glId mShader;
};

void Shader::create() {
	mProgram = glCreateProgram();
	if (!mProgram) throw std::runtime_error("glCreateProgram failed");

	ShaderHandle vertexShader(mProgram, compile(GL_VERTEX_SHADER, load(RES_DIR + std::string("basic.vert"))));
	ShaderHandle fragmentShader(mProgram, compile(GL_FRAGMENT_SHADER, load(RES_DIR + std::string("basic.frag"))));

	glLinkProgram(mProgram);

	int  success = 0;
	glGetProgramiv(mProgram, GL_LINK_STATUS, &success);
	if (!success) {
		int length;
		glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &length);
		std::unique_ptr<char[]> error(new char[length]);
		glGetProgramInfoLog(mProgram, length, NULL, error.get());
		throw std::runtime_error(("program link error\n\n" + std::string(error.get())).c_str());
	}
}

std::string Shader::load(const std::string &shaderFilePath) const {
	std::ifstream stream(shaderFilePath);
	if (!stream) throw std::runtime_error(("shader file missing: " + shaderFilePath).c_str());

	std::string source;
	std::string line;
	while (std::getline(stream, line))
		source += line + "\n";

	stream.close();
	return source;
}

util::glId Shader::compile(const util::glId shaderType, const std::string &shaderSource) const {
	util::glId shader = 0;
	shader = glCreateShader(shaderType);
	if (!shader) throw std::runtime_error("failed to create shader");

	const char *shaderCode = shaderSource.c_str();
	glShaderSource(shader, 1, &shaderCode, NULL);

	glCompileShader(shader);

	int  success = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		int length;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
		std::vector<char> errorMessage(length);
		glGetShaderInfoLog(shader, length, NULL, &errorMessage[0]);
		throw std::runtime_error(("shader compile error\n\n" + std::string(&errorMessage[0])).c_str());
	}

	return shader;
}
}
