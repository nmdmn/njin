#include "attribute.h"

namespace core::render {
template<> void Channel<glm::vec3>::apply(int stride, unsigned int offset, unsigned int index) {
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, stride, (void*) offset);
}

template<> void Channel<glm::vec4>::apply(int stride, unsigned int offset, unsigned int index) {
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE, stride, (void*) offset);
}
}
