#include "mesh.h"

#include "core/asset/config.h"

namespace njAsset = core::asset;

namespace core::render {

void Mesh::create() {
	auto &config = njAsset::Config::get();
	const float size = 2.0f;
	/*Vertex vertices[] = {
		{glm::vec3(-size, -size,  size), config.fgColor},
		{glm::vec3(size, -size,  size), config.fgColor},
		{glm::vec3(-size,  size,  size), config.fgColor},
		{glm::vec3(size,  size,  size), config.fgColor},
		{glm::vec3(-size, -size, -size), config.fgColor},
		{glm::vec3(size, -size, -size), config.fgColor},
		{glm::vec3(-size,  size, -size), config.fgColor},
		{glm::vec3(size,  size, -size), config.fgColor},
	};

	unsigned int indices[] = {
		0, 1, 2,  2, 1, 3,
		4, 6, 5,  6, 7, 5,
		2, 3, 7,  2, 7, 6,
		0, 5, 1,  0, 4, 5,
		0, 2, 4,  4, 2, 6,
		1, 5, 3,  5, 7, 3
	};*/

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	const float PI = static_cast<float>(M_PI);
	const float radius = 3.0f;
	const int resolution = 16;
	for (unsigned int i = 0; i <= resolution; ++i) {
		for (unsigned int j = 0; j < resolution; ++j) {
			float theta = i * PI / resolution;
			float phi = j * 2 * PI / resolution;
			float x = radius * std::cos(phi) * std::sin(theta);
			float y = radius * std::cos(theta);
			float z = radius * std::sin(phi) * std::sin(theta);
			vertices.push_back(Vertex{glm::vec3(x, y, z), glm::vec3(x, y, z)});
		}
	}
	for (unsigned int i = 0; i < resolution; ++i) {
		for (unsigned int j = 0; j <= resolution; ++j) {
			indices.push_back((i * resolution) + (j % resolution));
			indices.push_back(((i + 1) * resolution) + (j % resolution));
		}
	}

	mAttribute.create();
	mAttribute.loadVertices(vertices); //TODO this could be templated like loadVertices<decltype(Vertex::position),...>(vertices);
	mAttribute.loadIndices(indices);

	njRender::Layout<decltype(Vertex::position), decltype(Vertex::color)> layout;
	mAttribute.layout(layout);
}

void Mesh::draw() {
	mAttribute.draw(GL_TRIANGLE_STRIP);
}
}
