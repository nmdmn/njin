#include "window.h"

#include "core/asset/config.h"

namespace njAsset = core::asset;

namespace core::render {
std::vector<core::logic::WindowListener*> Window::mListeners;

static void callback_WindowSizeCallback(GLFWwindow* window, int width, int height) {
	Window::notifySizeChanged(width, height);
	glViewport(0, 0, width, height);
}

Window::Window() {}

Window::~Window() {}

Window& Window::get() {
	static Window instance;
	return instance;
}

void Window::create(int width, int height) {
	if (!glfwInit()) throw std::runtime_error("glfwInit failed");

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	mMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwWindowHint(GLFW_RED_BITS, mMode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mMode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mMode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mMode->refreshRate);

	glfwWindowHint(GLFW_SAMPLES, 8);

	if (width + height == 0) {
		width = mMode->width;
		height = mMode->height;
		glfwWindowHint(GLFW_DECORATED, false);
	}

	mWindow = smart_GLFWwindow(glfwCreateWindow(width, height, "Njin v0.1.0", nullptr, nullptr));
	if (!mWindow) { glfwTerminate(); throw std::runtime_error("glfwCreateWindow failed"); }

	glfwMakeContextCurrent(mWindow.get());
	glfwSetWindowSizeCallback(mWindow.get(), callback_WindowSizeCallback);

	if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) throw std::runtime_error("gladLoadGLLoader failed");


	auto &config = njAsset::Config::get();
	glClearColor(config.bgColor.r, config.bgColor.g, config.bgColor.b, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE /*GL_FILL*/);
}

bool Window::isClose() const {
	return glfwWindowShouldClose(mWindow.get());
}

void Window::clear() const {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::swapBuffer() const {
	glfwSwapBuffers(mWindow.get());
}

void Window::pollEvents() const {
	glfwPollEvents();
}

void Window::addListener(core::logic::WindowListener *listener) {
	mListeners.push_back(listener);
}

void Window::notifySizeChanged(int width, int height) {
	for (auto listener : mListeners) {
		listener->onSizeChanged(width, height);
	}
}

GLFWwindow* Window::getWindow() const {
	return mWindow.get();
}

const GLFWvidmode* Window::getMode() const {
	return mMode;
}
}
