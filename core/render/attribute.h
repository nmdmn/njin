#pragma once

#include "core/util/forward.h"

namespace core::render {
template<typename type_Channel> struct Channel {
	void apply(int stride, unsigned int offset, unsigned int index);
};

template<typename... type_Attributes> class Layout {
public:
	void setup(unsigned int stride, unsigned int offset = 0, unsigned int index = 0) {
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
};

template<typename type_Actual, typename... type_Attributes>
class Layout<type_Actual, type_Attributes...> : Layout<type_Attributes...> {
public:
	void setup(unsigned int stride, unsigned int offset = 0, unsigned int index = 0) {
		Channel<type_Actual> channel;
		channel.apply(stride, offset, index);
		Layout<type_Attributes...>::setup(stride, offset + sizeof(type_Actual), ++index);
	}
};

class Attribute {
public:
	void create() {
		glGenVertexArrays(1, &mVao);
		glBindVertexArray(mVao);

		glGenBuffers(1, &mVbo);
		glBindBuffer(GL_ARRAY_BUFFER, mVbo);

		glGenBuffers(1, &mEbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEbo);
	}

	template<typename type_Vertex> void loadVertices(const std::vector<type_Vertex> &vertices) {
		mStride = sizeof(type_Vertex);
		glBufferData(GL_ARRAY_BUFFER, mStride * vertices.size(), (void*) &vertices[0], GL_STATIC_DRAW);
	}

	template<typename type_Index> void loadIndices(const std::vector<type_Index> &indices) {
		mCount = indices.size();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(type_Index) * indices.size(), (void*) &indices[0], GL_STATIC_DRAW);
	}

	template<typename... type_Attributes> void layout(Layout<type_Attributes...> layout) {
		layout.setup(mStride);
	}

	void draw(unsigned int glMode) {
		glBindVertexArray(mVao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEbo);
		glDrawElements(glMode, mCount, GL_UNSIGNED_INT, 0);
	}

private:
	util::glId mVao = 0;
	util::glId mVbo = 0;
	util::glId mEbo = 0;

	unsigned int mCount = 0;
	unsigned int mStride = 0;
};
}
