#pragma once

#include "core/util/forward.h"
#include "core/render/attribute.h"

namespace njRender = core::render;

namespace core::render {
class Mesh {
public:
	struct Vertex {
		glm::vec3 position;
		glm::vec3 color;
	};

public:
	void create();
	void draw();

private:
	njRender::Attribute mAttribute;
};
}
