#pragma once

#include "core/util/forward.h"
#include "core/render/uniform.h"

namespace njRender = core::render;

namespace core::render {
class Shader {
public:
	void create();
	void use() { glUseProgram(mProgram); }
	template<typename T> void set(const njRender::Uniform<T> &uniform) const;

private:
	std::string load(const std::string &shaderFilePath) const;
	util::glId compile(const util::glId shaderType, const std::string &shaderSource) const;

private:
	util::glId mProgram = 0;
};

template<> inline void Shader::set(const njRender::Uniform<glm::vec4>& uniform) const {
	util::glId location = glGetUniformLocation(mProgram, uniform.mName.c_str()); // TODO cache the location, or specify the location
	glUniform4f(location, uniform.mData.r, uniform.mData.g, uniform.mData.b, uniform.mData.a);
}

template<> inline void Shader::set(const njRender::Uniform<glm::mat4>& uniform) const {
	util::glId location = glGetUniformLocation(mProgram, uniform.mName.c_str()); // TODO 2nd! cache the location, or specify the location	
	glUniformMatrix4fv(location, 1, GL_FALSE, &(uniform.mData[0][0]));
}
}
