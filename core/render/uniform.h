#pragma once

namespace core::render {
template<typename T> class Uniform {
public:
	void set(const std::string &name);
	void operator=(T data);

public:
	T mData;
	std::string mName;
};

template<typename T> inline void Uniform<T>::set(const std::string &name) {
	mName = name;
}

template<typename T> inline void Uniform<T>::operator=(T data) {
	mData = data;
}
}
