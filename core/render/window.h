#pragma once

#include "core/util/forward.h"
#include "core/logic/window_listener.h"

namespace core::render {
class Window {
private:
	struct destroy_GLFWwindow { void operator()(GLFWwindow *p) { glfwDestroyWindow(p); } };
	typedef std::unique_ptr<GLFWwindow, destroy_GLFWwindow> smart_GLFWwindow;
	typedef const GLFWvidmode *raw_GLFWvidmode;

public:
	~Window();
	static Window& get();

	void create(int width = 0, int height = 0);
	bool isClose() const;
	void clear() const;
	void swapBuffer() const;
	void pollEvents() const;
	void addListener(core::logic::WindowListener *listener);
	static void notifySizeChanged(int width, int height);

	GLFWwindow* getWindow() const;
	const GLFWvidmode* getMode() const;

private:
	Window();
	Window(Window const&) {}
	Window& operator=(Window const&) {}

private:
	smart_GLFWwindow mWindow;
	raw_GLFWvidmode mMode;
	static std::vector<core::logic::WindowListener*> mListeners;
};
}
