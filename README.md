# NJIN

Hello there fellow warrior! This is nmd's 3D sandbox, made for playing with basic OpenGL.

### Table of Contents
* [Dependencies](#DEPENDENCIES)
* [Build](#BUILD)

## DEPENDENCIES

| NAME | DESCRIPTION |
|--|--|
| Opengl | Portable 2D/3D graphics API |
| GLFW  | Window handling framework |
| GLAD | GL extension loading library |
| GLM | Usefull math library |
| imgui | Pretty GUI framework |
| Loguru | Logging library |

If you do not want to use prebuilt dependencies, either
> define enviroment variables points to corresponding library: **GLFW_LOCATION**, **GLM_LOCATION**

> or modify **njin/cmake/find<_SOME_FANCY_LIBRARY_>.cmake** to your liking

## BUILD

~~~~
mkcd build
cmake ..
~~~~
