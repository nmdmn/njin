find_path(GLAD_INCLUDE_DIR 
    NAMES
        glad/glad.h
    PATHS
        "${CMAKE_SOURCE_DIR}/deps"
    DOC 
        "The directory where glad/glad.h resides"
)

find_package_handle_standard_args(
    GLAD
    DEFAULT_MSG
    GLAD_INCLUDE_DIR
)

mark_as_advanced(GLAD_INCLUDE_DIR)

if(GLAD_FOUND)
  set(GLAD_INCLUDE_DIRS ${GLAD_INCLUDE_DIR})
  file(GLOB_RECURSE GLAD_SOURCE "${GLAD_INCLUDE_DIR}/*.c")
endif()
