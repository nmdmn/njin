find_path(GLM_INCLUDE_DIR 
    NAMES
        glm/glm.hpp
	HINTS
		"/usr/local/include"
		"/usr/include"
        "$ENV{GLM_LOCATION}/include"
    PATHS
        "${CMAKE_SOURCE_DIR}/deps"
    DOC 
        "The directory where glm/glm.hpp resides"
)

find_package_handle_standard_args(
    GLM
    DEFAULT_MSG
    GLM_INCLUDE_DIR
)

mark_as_advanced(GLM_INCLUDE_DIR)

if(GLM_FOUND)
  set(GLM_INCLUDE_DIRS ${GLM_INCLUDE_DIR})
endif()
