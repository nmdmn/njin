find_path(IMGUI_INCLUDE_DIR 
    NAMES
        imgui.h
    PATHS
        "${CMAKE_SOURCE_DIR}/deps/imgui"
    DOC 
        "The directory where imgui/imgui.h resides"
)

find_package_handle_standard_args(
    imgui
    DEFAULT_MSG
    IMGUI_INCLUDE_DIR
)

mark_as_advanced(IMGUI_INCLUDE_DIR)

if(IMGUI_FOUND)
  set(IMGUI_INCLUDE_DIRS ${IMGUI_INCLUDE_DIR})
  file(GLOB_RECURSE IMGUI_SOURCE "${IMGUI_INCLUDE_DIR}/*.cpp")
endif()
