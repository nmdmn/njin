find_path(LOGURU_INCLUDE_DIR 
    NAMES
        loguru.hpp
    PATHS
        "${CMAKE_SOURCE_DIR}/deps/loguru"
    DOC 
        "The directory where loguru/loguru.hpp resides"
)

find_package_handle_standard_args(
    Loguru
    DEFAULT_MSG
    LOGURU_INCLUDE_DIR
)

mark_as_advanced(LOGURU_INCLUDE_DIR)

if(LOGURU_FOUND)
  set(LOGURU_INCLUDE_DIRS ${LOGURU_INCLUDE_DIR})
  file(GLOB_RECURSE LOGURU_SOURCE "${LOGURU_INCLUDE_DIR}/*.cpp")
endif()
