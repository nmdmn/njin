find_path(GLFW_INCLUDE_DIR 
    NAMES
		GLFW/glfw3.h
    HINTS        
		$ENV{GLFW_LOCATION}/include
    PATHS
		"/usr/local/include"
        "/usr/include/GL"
		"/usr/include"
		"${CMAKE_SOURCE_DIR}/deps"
    DOC 
        "The directory where GLFW/glfw3.h resides"
)

find_library(GLFW_LIBRARY
	NAMES 
		glfw
        glfw3
    HINTS
		"$ENV{GLFW_LOCATION}/lib"
		"$ENV{GLFW_LOCATION}/lib/x11"
		"$ENV{GLFW_LOCATION}/lib"
		"$ENV{GLFW_LOCATION}/lib/x64"
		"$ENV{GLFW_LOCATION}/lib-msvc110"
		"$ENV{GLFW_LOCATION}/lib-vc2012"
    PATHS		
		"$ENV{PROGRAMFILES}/GLFW/lib"
		"/usr/lib64"
		"/usr/lib"
		"/usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}"
		"/usr/local/lib64"
		"/usr/local/lib"
		"/usr/local/lib/${CMAKE_LIBRARY_ARCHITECTURE}"
		"${CMAKE_SOURCE_DIR}/deps/_prebaked"
    DOC 
        "The GLFW library"
)

find_package_handle_standard_args(
    GLFW
    DEFAULT_MSG
    GLFW_INCLUDE_DIR
    GLFW_LIBRARY
)

mark_as_advanced(GLFW_INCLUDE_DIR GLFW_LIBRARY)

if(GLFW_FOUND)
    set(GLFW_INCLUDE_DIRS ${GLFW_INCLUDE_DIR})
    set(GLFW_LIBRARIES    ${GLFW_LIBRARY})
endif()
