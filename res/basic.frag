#version 330 core

in vec3 iColor;

out vec4 oColor;

uniform vec4 uColor;

void main()
{
   oColor = mix(vec4(iColor, 1.0), uColor, 0.80);
}
