#include <core/asset/config.h>
#include <core/render/window.h>
#include <core/render/shader.h>
#include <core/render/mesh.h>
#include <core/render/uniform.h>
#include <core/item/camera.h>

#include <loguru/loguru.hpp>

namespace njAsset = core::asset;
namespace njRender = core::render;
namespace njItem = core::item;

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

int main(int argc, char **argv) {

	try {
		njRender::Window& window = njRender::Window::get();
		window.create(WINDOW_WIDTH, WINDOW_HEIGHT);

		njRender::Shader shader;
		shader.create();

		njRender::Mesh mesh;
		mesh.create();

		njRender::Uniform<glm::vec4> uColor;
		njRender::Uniform<glm::mat4> uModel;


		//material
		uColor.set("uColor");

		//model
		uModel.set("uModel");

		njItem::Camera camera;
		camera.create();
		camera.setView(glm::lookAt(glm::vec3(4.0f, 4.0f, 4.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
		camera.setProjection(glm::perspective(45.0f, (float) WINDOW_WIDTH / WINDOW_HEIGHT, 0.1f, 1000.0f));
		window.addListener(&camera);

		auto &config = njAsset::Config::get();
		uColor = glm::vec4(config.fgColor, 1.0f);

		loguru::init(argc, argv);
		LOG_SCOPE_FUNCTION(INFO);

		ImGui::CreateContext();
		ImGui_ImplGlfwGL3_Init(window.getWindow(), true);
		ImGui::StyleColorsDark();

		ImGui::GetIO().IniFilename = NULL;

		ImGuiStyle& style = ImGui::GetStyle();
		style.Colors[ImGuiCol_WindowBg] = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
		style.Colors[ImGuiCol_FrameBg] = ImVec4(0.0f, 0.0f, 0.0f, 0.25f);
		style.Colors[ImGuiCol_Border] = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
		style.Colors[ImGuiCol_MenuBarBg] = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
		//style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
		//style.Colors[ImGuiCol_ResizeGrip] = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
		//style.WindowPadding = ImVec2(6, 4);
		style.WindowRounding = 0.0f;
		//style.FramePadding = ImVec2(5, 2);
		//style.FrameRounding = 0.0f;
		//style.ItemSpacing = ImVec2(7, 1);
		//style.ItemInnerSpacing = ImVec2(1, 1);
		//style.TouchExtraPadding = ImVec2(0, 0);
		//style.IndentSpacing = 6.0f;
		//style.ScrollbarSize = 12.0f;
		//style.ScrollbarRounding = 16.0f;
		//style.GrabMinSize = 20.0f;
		//style.GrabRounding = 2.0f;

		while (!window.isClose()) {
			window.clear();

			ImGui_ImplGlfwGL3_NewFrame();

			shader.use();

			double now = glfwGetTime();
			unsigned long long time = static_cast<unsigned long long>(std::round(glfwGetTime() * 1000)); //jezusfasza gyerek... TODO!
			float stopper = (time % 8000) / 8000.0f;

			const float PI = static_cast<float>(M_PI);
			float alpha = 2 * PI * stopper;

			uModel = glm::rotate(alpha, glm::vec3(0.0f, 1.0f, 0.0f));


			shader.set(uColor);
			shader.set(uModel);

			camera.update(shader);

			mesh.draw();

			{
				glm::vec3 input = uColor.mData;

				ImGuiWindowFlags window_flags = 0;
				window_flags |= ImGuiWindowFlags_NoTitleBar;
				window_flags |= ImGuiWindowFlags_NoScrollbar;
				window_flags |= ImGuiWindowFlags_MenuBar;
				window_flags |= ImGuiWindowFlags_NoMove;
				window_flags |= ImGuiWindowFlags_NoResize;
				window_flags |= ImGuiWindowFlags_NoCollapse;
				window_flags |= ImGuiWindowFlags_NoNav;
				bool p_open = false;
				ImGui::Begin("Panel", &p_open, window_flags);
				ImGui::SetWindowPos(ImVec2(0.f, -16.5f));
				ImGui::SetWindowSize(ImVec2(250.f, WINDOW_HEIGHT));

				ImGui::SliderFloat("r", &input.r, 0.0f, 1.0f);
				ImGui::SliderFloat("g", &input.g, 0.0f, 1.0f);
				ImGui::SliderFloat("b", &input.b, 0.0f, 1.0f);
				uColor = glm::vec4(input, 1.0f);

				static float values[90] = {0};
				static int values_offset = 0;
				static float refresh_time = 0.0f;
				while (refresh_time < ImGui::GetTime()) // Create dummy data at fixed 60 hz rate for the demo
				{
					static float phase = 0.0f;
					values[values_offset] = cosf(phase);
					values_offset = (values_offset + 1) % IM_ARRAYSIZE(values);
					phase += 0.10f*values_offset;
					refresh_time += 1.0f / 60.0f;
				}
				ImGui::PlotLines("IZE", values, IM_ARRAYSIZE(values), values_offset, "ide is irhatok", -1.0f, 1.0f, ImVec2(0, 45));

				ImGui::Text("ms/f: %.3f\nfps : %.1f", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

				ImGui::End();
			}

			//ImGui::ShowDemoWindow();

			ImGui::Render();
			ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
			window.swapBuffer();
			window.pollEvents();
		}		
		ImGui_ImplGlfwGL3_Shutdown();
		ImGui::DestroyContext();

	} catch (const std::exception &e) { std::cerr << e.what() << std::endl; return EXIT_FAILURE; }
	return EXIT_SUCCESS;
}
